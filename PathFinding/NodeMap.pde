/********************
  Énumération des directions
  possibles
  
********************/
enum Direction {
  EAST, SOUTH, WEST, NORTH
}

/********************
  Représente une carte de cellule permettant
  de trouver le chemin le plus court entre deux cellules
  
********************/
class NodeMap extends Matrix {
  Node start,Waypoint1,Waypoint2;
  Node end;
  
  ArrayList <Node> path;
  
  boolean debug = false;
  
  NodeMap (int nbRows, int nbColumns) {
    super (nbRows, nbColumns);
    
  }
  
  NodeMap (int nbRows, int nbColumns, int bpp, int width, int height) {
    super (nbRows, nbColumns, bpp, width, height);
  }
  
  void init() {
    
    cells = new ArrayList<ArrayList<Cell>>();
    
    for (int j = 0; j < rows; j++){
      // Instanciation des rangees
      cells.add (new ArrayList<Cell>());
      
      for (int i = 0; i < cols; i++) {
        Cell temp = new Node(i * cellWidth, j * cellHeight, cellWidth, cellHeight);
        
        // Position matricielle
        temp.i = i;
        temp.j = j;
        
        cells.get(j).add (temp);
      }
    }
    
    println ("rows : " + rows + " -- cols : " + cols);
  }
  
  /*
    Configure la cellule de départ
  */
  void setStartCell (int i, int j) {
    
    if (start != null) {
      start.isStart = false;
      start.setFillColor(baseColor);
    } 
    
    start = (Node)cells.get(j).get(i);
    start.isStart = true;
    
    start.setFillColor(color (0, 255, 0));
  }
  
  /*
    Configure la cellule de fin
  */
  void setEndCell (int i, int j) {
    
    if (end != null) {
      end.isEnd = false;
      end.setFillColor(baseColor);
    }
    
    end = (Node)cells.get(j).get(i);
    end.isEnd = true;
    
    end.setFillColor(color (255, 0, 0));
  }
  
  /** Met a jour les H des cellules
  doit etre appele apres le changement du noeud
  de debut ou fin
  */
  void updateHs() {
    for (int j = 0; j < rows; j++) {
      for (int i = 0; i < cols; i++) {
        Node current = (Node)cells.get(j).get(i); 
        current.setH( calculateH(current));
      }
    }
  }
  
  // Permet de generer aleatoirement le cout de deplacement
  // entre chaque cellule
  void randomizeMovementCost() {
    for (int j = 0; j < rows; j++) {
      for (int i = 0; i < cols; i++) {
        
        int cost = parseInt(random (0, cols)) + 1;
        
        Node current = (Node)cells.get(j).get(i);
        current.setMovementCost(cost);
       
      }
    }
  }
  
  // Permet de generer les voisins de la cellule a la position indiquee
  void generateNeighbours(int i, int j) {
    Node c = (Node)getCell (i, j);
    if (debug) println ("Current cell : " + i + ", " + j);
    
    
    for (Direction d : Direction.values()) {
      Node neighbour = null;
      
      switch (d) {
        case EAST :
          if (i < cols - 1) {
            if (debug) println ("\tGetting " + d + " neighbour for " + i + ", " + j);
            neighbour = (Node)getCell(i + 1, j);
          }
          break;
        case SOUTH :
          if (j < rows - 1) {
            if (debug) println ("\tGetting " + d + " neighbour for " + i + ", " + j);
            neighbour = (Node)getCell(i, j + 1);
          }
          break;
        case WEST :
          if (i > 0) {
            if (debug) println ("\tGetting " + d + " neighbour for " + i + ", " + j);
            neighbour = (Node)getCell(i - 1, j);
          }
          break;
        case NORTH :
          if (j > 0) {
            if (debug) println ("\tGetting " + d + " neighbour for " + i + ", " + j);
            neighbour = (Node)getCell(i, j - 1);
          }
          break;
      }
      
      if (neighbour != null) {
        if (neighbour.isWalkable) {
          c.addNeighbour(neighbour);
        }
      }
    }
  }
  
  /**
    Génère les voisins de chaque Noeud
  */
  void generateNeighbourhood() {
    for (int j = 0; j < rows; j++) {
      for (int i = 0; i < cols; i++) {

        generateNeighbours(i, j);
      }
    }
  }
  
  /*
    Permet de trouver le chemin le plus court entre
    deux cellules
  */
  void findAStarPath () {
    // TODO : Complétez ce code
    if (start == null || end == null) {
      println ("No start and no end defined!");
      return;
    }
    ArrayList<Node> closeSet = new ArrayList<Node>();
    ArrayList<Node> openSet = new ArrayList<Node>();
    ArrayList<Node> neighborList = new ArrayList<Node>();

    openSet.add(start);
    Node current;
    boolean finish = false;
    // TODO : Complétez ce code
    //Tant que la « open list » contient des nœuds
    while(openSet.size() > 0 && finish == false)
    {
      //Trouver dans la « open list » le nœud qui a la plus petite valeur F et l’assigner comme nœud actif
      current = getLowestCost(openSet);
      
      //Si le nœud actif est le nœud final, on a trouvé le chemin
      if(current == end)
      {
        finish = true;
       // end.setParent(current);
        generatePath();
      }
      else
      {
        //Retirer le nœud actif de la « open list »
        openSet.remove(current);
        //Ajouter le nœud actif dans la « closed list »
        closeSet.add(current);
        
        //Pour chaque voisin du nœud actif
        neighborList = current.getNeighbors();
        for(int i = 0; i < neighborList.size(); i++)
        {
          Node neighbor = neighborList.get(i);
          
          //Si le voisin est dans la « closed list »
          if(closeSet.contains(neighbor))
          {
            //Passer au suivant
            continue;
          }
          else
          {
            //Calculer une valeur G'
            int deplacement = calculateCost(current, neighbor);
            int tempG = current.getG() + deplacement;
            
            //Si le voisin n’est pas dans la « open list »
            if(!openSet.contains(neighbor))
            {
              //Ajouter le voisin dans la « open list »
              openSet.add(neighbor);
            }
            else if(tempG >= neighbor.getG())
            {
              //Passer au suivant
              continue;
            }
            neighbor.setParent(current);
          }
        }
      }
    }
    
    
    // Appelez generatePath si le chemin est t
    // TODO : Complétez ce code
    
  }
  
  /*
    Permet de générer le chemin une fois trouvée
  */
  void generatePath () {
    // TODO : Complétez ce code
    
    Node current = end;
    int i = 0;
    while(current != start)
    {
      current.selected = true;
      if(current != start && current != end)
        current.setFillColor(255);
      current = current.getParent();
    }
  }
  
  /**
  * Cherche et retourne le noeud le moins couteux de la liste ouverte
  * @return Node le moins couteux de la liste ouverte
  */
  private Node getLowestCost(ArrayList<Node> openSet) {
    // TODO_Done : Complétez ce code
    int lowest = 0;
    for(int i = 0; i < openSet.size();i++)
    {
      if(openSet.get(i).getF() < openSet.get(lowest).getF())
      {
        lowest = i;
      }
    }
    Node n = openSet.get(lowest);
    openSet.remove(lowest);
    return n;
  }
  

  
 /**
  * Calcule le coût de déplacement entre deux noeuds
  * @param nodeA Premier noeud
  * @param nodeB Second noeud
  * @return
  */
  private int calculateCost(Cell nodeA, Cell nodeB) {
    // TODO : Complétez ce code
    int dx = nodeA.i - nodeB.i;
    int dy = nodeA.j - nodeB.j;
    return abs(dx) + abs(dy);
  }
  
  /**
  * Calcule l'heuristique entre le noeud donnée et le noeud finale
  * @param node Noeud que l'on calcule le H
  * @return la valeur H
  */
  private int calculateH(Cell node) 
  {
    // TODO_DONE : Complétez ce code
    int dx = end.i - node.i;
    int dy = end.j - node.j;
    return abs(dx) + abs(dy);
  }
  
  String toStringFGH() {
    String result = "";
    
    for (int j = 0; j < rows; j++) {
      for (int i = 0; i < cols; i++) {

        Node current = (Node)cells.get(j).get(i);
        
        result += "(" + current.getF() + ", " + current.getG() + ", " + current.getH() + ") ";
      }
      
      result += "\n";
    }
    
    return result;
  }
  
  // Permet de créer un mur à la position _i, _j avec une longueur
  // et orientation données.
  void makeWall (int _i, int _j, int _length, boolean _vertical) {
    int max;
    
    if (_vertical) {
      max = _j + _length > rows ? rows: _j + _length;  
      
      for (int j = _j; j < max; j++) {
        ((Node)cells.get(j).get(_i)).setWalkable (false, 0);
      }       
    } else {
      max = _i + _length > cols ? cols: _i + _length;  
      
      for (int i = _i; i < max; i++) {
        ((Node)cells.get(_j).get(i)).setWalkable (false, 0);
      }     
    }
  }  
}
