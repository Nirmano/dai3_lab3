NodeMap worldMap;

int deltaTime = 0;
int previousTime = 0;
int rowsS,colsS,rowsS2,colsS2,rowsS3,colsS3,rowsE,colsE;
int mapRows = 100;
int mapCols = 100;
boolean random = true;

color baseColor = color (0, 127, 0);

void setup () {
  //size (420, 420);
  fullScreen();
  if(random)
  {
    rowsS = int(random(100));
    colsS = int(random(100));
    rowsE = int(random(100));
    colsE = int(random(100));
  }
  else
  {
    rowsS = int(11);
    colsS = int(88);
    rowsS2 = int(56);
    colsS2 = int(90);
    rowsS3 = int(90);
    colsS3 = int(56);
    rowsE = int(88);
    colsE = int(11);
  }
  initMap();
}

void draw () {
  deltaTime = millis () - previousTime;
  previousTime = millis();
  
  update(deltaTime);
  display();
}

void update (float delta) {
  
}

void display () {
  
  if (worldMap.path != null) {
    for (Cell c : worldMap.path) {
      c.setFillColor(color (127, 0, 0));
    }
  }
  worldMap.display();
}

void initMap () {
  worldMap = new NodeMap (mapRows, mapCols); 
  
  worldMap.setBaseColor(baseColor);
if(random)
{
  worldMap.setStartCell(rowsS, colsS);
  worldMap.setEndCell(rowsE, colsE);
    worldMap.updateHs();
  worldMap.generateNeighbourhood();
  worldMap.findAStarPath();
}
else
{

  worldMap.setStartCell(rowsS, colsS);
  worldMap.setEndCell(rowsS2, colsS2);
  worldMap.updateHs();
  worldMap.generateNeighbourhood();
  worldMap.findAStarPath();

  worldMap.setStartCell(rowsS2, colsS2);
  worldMap.setEndCell(rowsS3, colsS3);
  worldMap.updateHs();
  println('A');
  worldMap.findAStarPath();
  println('B');
  worldMap.setStartCell(rowsS3, colsS3);
  worldMap.setEndCell(rowsE, colsE);
  worldMap.updateHs();
  worldMap.findAStarPath();
}

 // worldMap.makeWall (mapCols / 2, 0, 15, true);
 // worldMap.makeWall (mapCols / 2 - 9, 10, 10, false); //<>//
}

void keyPressed()
{
  if(key == 'r')
  {
    random = true;
    setup();
  }
  else if(key == 'w')
  {
    random = false;
    setup();
  }
}
